$fn=50;


fan_size = 140;

tofan_size = 160; //mm


module plug(){
hull(){
	linear_extrude(5)
	square([fan_size, fan_size]);

	translate([0,0,20])
		linear_extrude(5)
			square([tofan_size, tofan_size]);
	

}

translate([0,0,-20])
	linear_extrude(23)
		square([fan_size, fan_size]);
}

difference(){
	minkowski(){
		sphere(1);
		plug();
	}	
	
	plug();

	translate([tofan_size/2, tofan_size/2, 20])
		cylinder(10, 80, 80);
	
	translate([fan_size/2, fan_size/2, -22])
		cylinder(10, fan_size/2, fan_size/2);
	
	translate([tofan_size/2, tofan_size/2,0])
	for(i=[45:90:360]){
		echo(i);
		translate([(10+tofan_size/2)*sin(i), (10+tofan_size/2)*cos(i), 18])
		cylinder(h=10,r1=2,r2=2, $fn=6);
	}
}